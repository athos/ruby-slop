ruby-slop (4.9.1-1) UNRELEASED; urgency=medium

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 3.4.5-1.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Athos Ribeiro ]
  * New upstream version 4.9.1 (Closes: #996509)
  * Bump Standards-Version to 4.6.0
  * Update packaging to match gem2deb updates

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Wed, 20 Oct 2021 18:00:43 -0300

ruby-slop (4.6.2-1) unstable; urgency=medium

  * d/watch: use github as upstream
  * Add d/gbp.conf
  * New upstream version 4.6.2
  * d/control: Bump Standard-Version 4.2.1
  * d/control: Update Vcs-{Git,Browser}. use salsa
  * d/{copyright,control}: update uri, use https
  * d/{control,compat}: Bump compat 11

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 20 Dec 2018 17:42:00 +0900

ruby-slop (4.5.0-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Antonio Terceiro ]
  * Remove myself from Uploaders:

  [ Youhei SASAKI ]
  * New upstream version 4.5.0
  * Add patch: Remove git ls-files from gemspec
  * Bump debhelper >= 10
  * Bump Compat: 10
  * Bump Standard Version: 4.1.0

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 05 Sep 2017 12:57:36 +0900

ruby-slop (4.2.0-1) unstable; urgency=medium

  * Imported Upstream version 4.2.0
  * Update debian/rules: follow upstream file renaming
  * Bump Standard Version: 3.9.6

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 29 Jul 2015 12:42:47 +0900

ruby-slop (3.6.0-1) unstable; urgency=medium

  * Imported Upstream version 3.6.0

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 12 Aug 2014 03:40:47 +0900

ruby-slop (3.5.0-1) unstable; urgency=medium

  * New upstream release

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 12 May 2014 13:58:08 -0300

ruby-slop (3.4.7-1) unstable; urgency=medium

  [ Jonas Genannt ]
  * Team upload.
  * Imported Upstream version 3.4.7
  * d/control: bumped up standards version to 3.9.5 (no changes needed)
  * d/control: changed dependency from ruby1.8 to ruby

  [ Youhei SASAKI ]
  * Update debian/changelog

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 15 Dec 2013 22:43:38 +0900

ruby-slop (3.4.6-1) unstable; urgency=low

  * Imported Upstream version 3.4.6
  * Bump Standard Version: 3.9.4

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 05 Aug 2013 22:55:46 +0900

ruby-slop (3.4.5-1) unstable; urgency=low

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * Use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0 official URL for Format
    field

  [ Ondřej Surý ]
  * Imported Upstream version 3.4.5

 -- Ondřej Surý <ondrej@debian.org>  Mon, 27 May 2013 10:13:55 +0200

ruby-slop (3.4.3-1) experimental; urgency=low

  * Imported Upstream version 3.4.3
  * Update upstream url: use gemwatch
  * Install upstream changelog as CHANGES.md
  * Bump Standard version: 3.9.3

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 20 Jan 2013 06:53:09 +0900

ruby-slop (2.4.4-1) unstable; urgency=low

  * Initial release (Closes: #657765)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 29 Jan 2012 19:20:58 +0900
